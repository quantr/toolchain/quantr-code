// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from 'vscode';
import * as path from 'path';
import * as fs from 'fs';

// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
export function activate(context: vscode.ExtensionContext) {

	// Use the console to output diagnostic information (console.log) and errors (console.error)
	// This line of code will only be executed once when your extension is activated
	console.log('Congratulations, your extension "quantr-code" is now active!');

	// The command has been defined in the package.json file
	// Now provide the implementation of the command with registerCommand
	// The commandId parameter must match the command field in package.json
	let disposable = vscode.commands.registerCommand('quantr-code.helloWorld', () => {
		// The code you place here will be executed every time your command is executed

		// Display a message box to the user
		// vscode.window.showInformationMessage('Hello Shit from Quantr Code!');

		const cpuPanel = vscode.window.createWebviewPanel(
			'cpuPanel',
			'CPU',
			vscode.ViewColumn.Two,
			{
				localResourceRoots: [vscode.Uri.file(path.join(context.extensionPath, 'library'))]
			}
		);
		cpuPanel.webview.html = getWebviewContent(context.extensionPath, 'cpu.html');

		const memoryPanel = vscode.window.createWebviewPanel(
			'memoryPanel',
			'Memory',
			vscode.ViewColumn.Two,
			{}
		);
		memoryPanel.webview.html = getWebviewContent(context.extensionPath, 'memory.html');
	});

	context.subscriptions.push(disposable);
}

function getWebviewContent(extensionPath: string, filename: string) {
	const resourcePath = path.join(extensionPath, 'src', 'webview', filename);
	console.log('resourcePath=' + resourcePath);
	let html = fs.readFileSync(resourcePath, 'utf-8');
	return html;

}

// this method is called when your extension is deactivated
export function deactivate() { }
